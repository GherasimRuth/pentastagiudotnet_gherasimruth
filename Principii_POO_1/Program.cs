﻿using System;
using System.Collections.Generic;

namespace Principii_POO_1
{
    class Program
    {
        
       private static string afiseazaMenu()
        {
            Console.WriteLine("*********MENIU**********");
            Console.WriteLine("1. Adaugarea unui student");
            Console.WriteLine("2. Adaugarea unui profesor");
            Console.WriteLine("3. Afisarea tuturor persoanelor");
            Console.WriteLine("4. Afisarea studentilor");
            Console.WriteLine("5. Afisarea profesorilor");
            Console.WriteLine("6. Adaugarea unui student la n profesori");
            Console.WriteLine("7. Adaugarea unui profesor la n studenti");
            Console.WriteLine("x. Exit");
            Console.WriteLine("Dati optiunea: ");
            string optiune= Console.ReadLine();
            return optiune;
        }
       
        static void Main(string[] args)
        {
           List<Student> LStudent = new List<Student>();
           List<Profesor> LProfesor = new List<Profesor>();
            while (true)
            {
                switch (afiseazaMenu())
                {
                    case "1":
                        {
                            Console.WriteLine("1. Adaugarea unui student(nume, prenume, cnp, nr matricol, nota)");
                            string strInput = Console.ReadLine();
                            string[] data = strInput.Split();
                            Student s = new Student(data[0], data[1], data[2], Convert.ToInt32(data[3]), Convert.ToInt32(data[4]));
                            LStudent.Add(s);
                            break;
                        }
                    case "2":
                        {
                            Console.WriteLine("1. Adaugarea unui profesor(nume, prenume, cnp, disciplina)");
                            string strInput = Console.ReadLine();
                            string[] data = strInput.Split();
                            Profesor s = new Profesor(data[0], data[1], data[2], data[3]);
                            LProfesor.Add(s);
                            break;
                        }
                    case "3":
                        Console.WriteLine("Persoanele sunt:\n");
                        for (int i = 0; i < LStudent.Count; i++)
                            LStudent[i].afiseazaPar();
                        for (int i = 0; i < LProfesor.Count; i++)
                            LProfesor[i].afiseazaPar();
                        break;
                    case "4":
                        {
                            Console.WriteLine("Studentii sunt:\n");
                            for (int i = 0; i < LStudent.Count; i++)
                                LStudent[i].afiseaza();
                            break;
                        }
                    case "5":
                        {
                            Console.WriteLine("Profesorii sunt:\n");
                            for (int i = 0; i < LProfesor.Count; i++)
                                LProfesor[i].afiseaza();
                            break;
                        }
                    case "6":
                        {
                            Console.WriteLine(" Adaugarea unui student la un profesori");
                            Console.WriteLine("Dati numele profesorului: ");
                            string strNumeProfesor = Console.ReadLine();

                            Profesor profesor = null;
                            foreach(Profesor prof in LProfesor)
                            {
                                if(prof.GetNameP().Equals(strNumeProfesor))
                                {
                                    profesor = prof;
                                    break;
                                }
                            }
                            if(profesor == null)
                            {
                                Console.WriteLine("Profesorul nu a fost gasit in lista");
                                break;
                            }

                            Console.WriteLine("Dati numele studentului : ");
                            string strNumeStudent = Console.ReadLine();
                            Student student = null;
                            foreach(Student stud in LStudent)
                            {
                                if (stud.GetNameS().Equals(strNumeStudent))
                                {
                                    student = stud;
                                    break;
                                }
                            }

                            if (student == null)
                            {
                                Console.WriteLine("Studentul nu a fost gasit in lista");
                                break;
                            }
                            profesor.PAdaugaS(student);
                            profesor.afiseazaStudenti();
                            break;
                        }
                    case "7":
                        {
                            Console.WriteLine(" Adaugarea unui profesor la studenti");
                           
                            Console.WriteLine("Dati numele studentului : ");
                            string strNumeStudent = Console.ReadLine();
                            Student student = null;
                            foreach (Student stud in LStudent)
                            {
                                if (stud.GetNameS().Equals(strNumeStudent))
                                {
                                    student = stud;
                                    break;
                                }
                            }
                            if (student == null)
                            {
                                Console.WriteLine("Studentul nu a fost gasit in lista");
                                break;
                            }


                            Console.WriteLine("Dati numele profesorului: ");
                            string strNumeProfesor = Console.ReadLine();
                            Profesor profesor = null;
                            foreach (Profesor prof in LProfesor)
                            {
                                if (prof.GetNameP().Equals(strNumeProfesor))
                                {
                                    profesor = prof;
                                    break;
                                }
                            }
                            if (profesor == null)
                            {
                                Console.WriteLine("Profesorul nu a fost gasit in lista");
                                break;
                            }

                            

                            
                            student.SAdaugaP(profesor);
                            student.afiseazaProfesori();
                            break;
                        }
                    case "x":
                    case "X":
                        return;
                    default:
                        Console.WriteLine("default");
                        break;
                }
            }
            Console.ReadKey();
        }
    }
}
