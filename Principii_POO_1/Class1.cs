﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Principii_POO_1
{
    public abstract class Persoana
    {
        protected string Nume;
        protected string Prenume;
        protected string Cnp;

        public Persoana()
        {
            Nume = "";
            Prenume = "";
            Cnp = "";
        }
        public Persoana(string Nume, string Prenume, string Cnp)
        {
            if (Nume == "" || Nume.Length == 1)
                Console.WriteLine("Nume invalid!");
            else
                 this.Nume = Nume;
            if (Prenume == "" || Prenume.Length == 1)
                Console.WriteLine("Prenume invalid!");
            else
                this.Prenume = Prenume;
            if (Cnp == "" || Cnp.Length == 1)
                Console.WriteLine("Cnp invalid!");
            else
                this.Cnp = Cnp;
        }
        public void afiseazaPar()
        {
            Console.WriteLine(Nume + " " + Prenume + " " + Cnp + " " );
        }
        abstract public void afiseaza();
    }
    public class Student : Persoana
    {
        private int nrMat;
        private int nota;
        private List<Profesor> ListaP = new List<Profesor>();


        public Student() {}

        public Student(string Nume, string Prenume, string Cnp, int nrMat, int nota) : base(Nume, Prenume, Cnp)
        {
            if (nrMat == 0)
                Console.WriteLine("nr Matricol invalid");
            else
            this.nrMat = nrMat;

            if (nota == 0)
                Console.WriteLine("nr Nota invalida");
            else
                this.nota = nota;
        }
        public string GetNameS()
        {
            return this.Nume;
        }
        public void SAdaugaP(Profesor p)
        {
            ListaP.Add(p);
        }
        public void  afiseazaProfesori()
        {
            for (int i = 0; i < ListaP.Count; i++)
                ListaP[i].afiseaza();
        }
        override public void afiseaza()
        {
            Console.WriteLine(Nume + " " + Prenume + " " + Cnp + " " + nrMat + " " + nota);
        }

    }
    public class Profesor : Persoana
    {
        private string Disciplina;
        private List<Student> ListaS = new List<Student>();

        public Profesor(){}

        public Profesor(string Nume, string Prenume, string Cnp, string Disciplina) : base(Nume, Prenume, Cnp)
        {
            if (Disciplina=="" || Disciplina.Length<= 1)
                Console.WriteLine("Disciplina invalida");
            else
                this.Disciplina = Disciplina;
        }
         public string GetNameP()
        {
            return this.Nume;
        }
        public void PAdaugaS(Student s)
        {
            ListaS.Add(s);
        }
        public void afiseazaStudenti()
        {
            for (int i = 0; i < ListaS.Count; i++)
                ListaS[i].afiseaza();
        }
        override public void afiseaza()
        {
            Console.WriteLine(Nume + " " + Prenume + " " + Cnp + " " +Disciplina);
        }

    }
}
